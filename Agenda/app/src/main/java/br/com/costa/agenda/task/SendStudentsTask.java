package br.com.costa.agenda.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.List;

import br.com.costa.agenda.client.WebClient;
import br.com.costa.agenda.converter.StudentConverter;
import br.com.costa.agenda.dao.StudentDAO;
import br.com.costa.agenda.model.Student;

public class SendStudentsTask extends AsyncTask {

    private Context context;
    private ProgressDialog dialog;

    public SendStudentsTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(context, "Aguarde...", "Enviando dados dos alunos", true, true);
    }

    @Override
    protected Object doInBackground(Object[] params) {

        StudentDAO dao = new StudentDAO(context);
        List<Student> students = dao.read();

        StudentConverter converter = new StudentConverter();
        String json = converter.converterJson(students);

        WebClient client = new WebClient();
        String response = client.post(json);

        return response;
    }

    @Override
    protected void onPostExecute(Object o) {
        dialog.dismiss();
        String response = (String) o;
        Toast.makeText(context, "Enviando notas" + response, Toast.LENGTH_SHORT).show();
        super.onPostExecute(o);
    }
}
